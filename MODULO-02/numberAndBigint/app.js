"use strict";
/**
 * arquivo: numberBigint.ts
 * descrição: arquivo responsável por ensinar conceitos básicos sobre 'Tipo number e bigint'
 * data: 17/02/2021
 * author: Glaucia Lemos <@glaucia_lemos86>
 * doc referência: <number> https://www.typescriptlang.org/docs/handbook/basic-types.html#number
 * doc referência: <bigint> https://www.typescriptlang.org/docs/handbook/release-notes/typescript-3-2.html#bigint
 * Transpilação do arquivo: <Windows> CTRL + SHIFT + B -> tsc: build/watch
 */
//number exemple
let num1 = 23.0;
let num2 = 0x78CF;
let num3 = 0o577;
let num4 = 0b110001;
console.log('Ponto Flutuante...', num1);
console.log('Hexadecimal...', num2);
console.log('Octal...', num3);
console.log('Binário...', num4);
//bigint exemple
let big1 = 1314641165135135131313111111n;
let big2 = 9223372036854775811n;
let big3 = 0x200000000000000000000000000000000000000000000000n;
let big4 = 365375409332725729550921208179070754913983135744n;
console.log('número flutuante', big1);
console.log('Hexadecimal', big2);
console.log('Octal', big3);
console.log('Binário', big4);
