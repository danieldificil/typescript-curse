/* eslint-disable prettier/prettier */
/**
 * arquivo: typeAnnotation.ts
 * descrição: arquivo responsável por ensinar conceitos básicos sobre 'Type Annotation'
 * data: 16/02/2021
 * author: Glaucia Lemos <@glaucia_lemos86>
 * doc referência: https://www.typescriptlang.org/docs/handbook/2/everyday-types.html
 * Transpilação do arquivo: <Windows> CTRL + SHIFT + B -> tsc: build/watch
 */

//variables
let nome: string = "Daniel Araujo"
console.log(nome)

//arrays
let animais: string[] = ['Elephant','giraffe','hipoppotamus', 'rhino']
console.log(animais)

//objects
let car: {name: string,age:number,price:number}

car = {name: 'Toyota Hilux', age: 2022, price: 20000}
console.log(car)

//functions (params)
function multipliedNumbers(num1:number, num2:number)
{
    return num1 * num2

}

console.log(multipliedNumbers(2,5))